project_id = "tonal-plateau-212205"
region = "europe-west1"

name = "my-cluster"
description = "Test cluster to showcase CI/CD with k8s, Gitlab CI"
zone = "europe-west1-b"
initial_node_count = 2 # number of nodes in the cluster

ntw_name = "my-cluster-network" # VPC network name which will be created
